package nodescala

import scala.language.postfixOps
import scala.util.{Try, Success, Failure}
import scala.collection._
import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.async.Async.{async, await}
import org.scalatest._
import NodeScala._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class NodeScalaSuite extends FunSuite {
  def failAfterHalfSecond = Future {
    Thread.sleep(500)
    throw new RuntimeException("faal")
  }

  def resultAfterSecond = Future {
    Thread.sleep(600)
    "a"
  }


  test("A Future should always be completed") {
    val always = Future.always(517)

    assert(Await.result(always, 0 nanos) == 517)
  }

  test("A Future should never be completed") {
    val never = Future.never[Int]

    try {
      Await.result(never, 1 second)
      assert(false)
    } catch {
      case t: TimeoutException => // ok!
    }
  }

  test("all - happy") {
    val list = List(1, 2, 3, 4, 5)
    val fs = list.map { Future.always(_) }

    assert(Await.result(Future.all(fs), 1 second) === list)
  }

  test("one - with error") {
    val f = Promise.failed(new RuntimeException("pech")).future
    intercept[RuntimeException] {
      Await.result(f, 1 second)
    }
  }

  test("all - with error") {
    val one = Promise.failed(new RuntimeException("pech")).future
    val fs: List[Future[Int]] = List(one)
    val f = Future.all(fs)
    intercept[RuntimeException] {
      Await.result(f, 1 second)
    }
  }

  test("any - no error") {
    val fs = List(Future("a"), failAfterHalfSecond)
    assert(Await.result(Future.any(fs), 1 second) === "a")
  }

  test("any - with error") {
    val fs = List(resultAfterSecond, failAfterHalfSecond)
    val f = Future.all(fs)
    intercept[RuntimeException] {
      Await.result(f, 1 second)
    }
  }

  test("now - with result") {
    val f = Future("a")
    assert(f.now === "a")
  }

  test("now - no result") {
    intercept[NoSuchElementException] {
      println(resultAfterSecond.now)
    }
  }



  class DummyExchange(val request: Request) extends Exchange {
    @volatile var response = ""
    val loaded = Promise[String]()
    def write(s: String) {
      response += s
    }
    def close() {
      loaded.success(response)
    }
  }

  class DummyListener(val port: Int, val relativePath: String) extends NodeScala.Listener {
    self =>

    @volatile private var started = false
    var handler: Exchange => Unit = null

    def createContext(h: Exchange => Unit) = this.synchronized {
      assert(started, "is server started?")
      handler = h
    }

    def removeContext() = this.synchronized {
      assert(started, "is server started?")
      handler = null
    }

    def start() = self.synchronized {
      started = true
      new Subscription {
        def unsubscribe() = self.synchronized {
          started = false
        }
      }
    }

    def emit(req: Request) = {
      val exchange = new DummyExchange(req)
      if (handler != null) handler(exchange)
      exchange
    }
  }

  class DummyServer(val port: Int) extends NodeScala {
    self =>
    val listeners = mutable.Map[String, DummyListener]()

    def createListener(relativePath: String) = {
      val l = new DummyListener(port, relativePath)
      listeners(relativePath) = l
      l
    }

    def emit(relativePath: String, req: Request) = this.synchronized {
      val l = listeners(relativePath)
      l.emit(req)
    }
  }
  test("Server should serve requests") {
    val dummy = new DummyServer(8191)
    val dummySubscription = dummy.start("/testDir") {
      request => for (kv <- request.iterator) yield (kv + "\n").toString
    }

    // wait until server is really installed
    Thread.sleep(500)

    def test(req: Request) {
      val webpage = dummy.emit("/testDir", req)
      val content = Await.result(webpage.loaded.future, 2 second)
      val expected = (for (kv <- req.iterator) yield (kv + "\n").toString).mkString
      assert(content == expected, s"'$content' vs. '$expected'")
    }

    test(immutable.Map("StrangeRequest" -> List("Does it work?")))
    test(immutable.Map("StrangeRequest" -> List("It works!")))
    test(immutable.Map("WorksForThree" -> List("Always works. Trust me.")))

    dummySubscription.unsubscribe()
  }

}




