package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {
  property("If you insert any two elements into an empty heap, finding the minimum of the resulting heap should get the smallest of the two elements back.") = forAll { a: Int =>
    forAll { b: Int =>
      val h = insert(b, insert(a, empty))
      findMin(h) == Math.min(a, b)
    }
  }

  property("If you insert an element into an empty heap, then delete the minimum, the resulting heap should be empty.") = forAll { a: Int =>
    val h = insert(a, empty)
    val h2 = deleteMin(h)
    h2.equals(empty)
  }

//  property("If you insert an element lower than the previous minumum, that's the new minimum") = forAll { h: H =>
//    val newMinimum = Math.min(findMin(h) - 1, 0)
//    val newHeap = insert(newMinimum, h)
//    findMin(newHeap) == newMinimum
//  }

  property("If the previous minumum again, does not change the minimum") = forAll { h: H =>
    if(isEmpty(h)) {
      true
    } else {
      val a = findMin(h)
      val newHeap = insert(a, h)
      findMin(newHeap) == findMin(h)
    }
  }

  property("deleting one element cannot decrease the minimum") = forAll { h: H =>
    if(isEmpty(h)) {
      true
    } else {
      val h2 = deleteMin(h)
      isEmpty(h2) || findMin(h2) >= findMin(h)
    }
  }


//    if (ord.lteq(t1.x,t2.x)) Node(t1.x, t1.r+1, t2::t1.c) else Node(t2.x, t2.r+1, t1::t2.c)
//    if (ord.lteq(t1.x,t2.x)) Node(t1.x, t1.r+1, t1::t1.c) else Node(t2.x, t2.r+1, t2::t2.c)

  //1 meld 2 => 1, 1
  property("after a meld all properties hold") = forAll { h1: H =>
    forAll { h2: H =>
      val melding = meld(h1, h2)
      toList(melding).sorted == (toList(h1) ++ toList(h2)).sorted
    }
  }

  def toList(heap: H): List[Int] = {
    if(isEmpty(heap)) {
      Nil
    } else {
      findMin(heap) :: toList(deleteMin(heap))
    }
  }

  property("Finding a minimum of the melding of any two heaps should return a minimum of one or the other.") = forAll { h1: H =>
    forAll { h2: H =>
      if(isEmpty(h1) || isEmpty(h2)) {
        true
      } else {
        val melding = meld(h1, h2)
        findMin(melding) == Math.min(findMin(h1), findMin(h2))
      }
    }
  }


  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  lazy val genHeap: Gen[H] = for {
    k <- arbitrary[Int]
    lastHeap <- oneOf(const(empty), genHeap)
    newHeap <- oneOf(const(empty), const(insert(k, lastHeap)))
  } yield newHeap

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}
