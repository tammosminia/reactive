package calculator

import org.scalatest.FunSuite

class CalculatorTest extends FunSuite {
  import Calculator._

  val references: Map[String, Signal[Expr]] = Map(
    "a" -> Signal(Literal(1)),
    "b" -> Signal(Plus(Literal(2), Literal(3))),
    "c" -> Signal(Plus(Ref("a"), Literal(1)))
  )

  test("eval - 1") {
    assert(eval(Literal(1), Map()) === 1)
  }

  test("eval - 1 + 2") {
    assert(eval(Plus(Literal(1), Literal(2)), Map()) === 3)
  }

  test("eval - a") {
    assert(eval(Ref("a"), references) === 1)
  }

  test("eval - b") {
    assert(eval(Ref("b"), references) === 5)
  }

  test("eval - c") {
    assert(eval(Ref("c"), references) === 2)
  }

  test("eval - nonexisting variable") {
    assert(eval(Ref("z"), references).isNaN)
  }

  test("eval - circular dependencies") {
    val circularReferences: Map[String, Signal[Expr]] = Map("a" -> Signal(Ref("b")), "b" -> Signal(Plus(Ref("a"), Ref("a"))))
    assert(eval(Ref("a"), circularReferences).isNaN)
  }


}
