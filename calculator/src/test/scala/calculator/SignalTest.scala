package calculator

import org.scalatest.FunSuite

class SignalTest extends FunSuite {
  test("map - length") {
    val s = Signal("bla")
    val r: Signal[Int] = s.map { s => s.length }
    assert(r() === 3)
  }

  test("map - updating Var") {
    val s = Var(1)
    val r: Signal[Int] = s.map { _ + 1 }
    assert(r() === 2)
    s.update(5)
    assert(r() === 6)
  }

}
