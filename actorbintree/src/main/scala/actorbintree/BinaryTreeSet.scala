/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply
  
  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
//  var pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = {
    case op: Operation => root ! op
    case GC =>
      println("starting GC!")
      val newRoot = createRoot
      root ! CopyTo(newRoot)
      context.become(garbageCollecting(newRoot, Nil))
    case _ => ???
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef, pending: List[Operation]): Receive = {
    case CopyFinished =>
      println("done GC!")
      root = newRoot
      context.become(normal)
      pending.foreach { root ! _ } //stuur alle requests opnieuw naar mezelf
    case op: Operation => context.become(garbageCollecting(newRoot, pending :+ op))
  }

}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)   //we gaan alle subnodes af en die sturen een insert naar treeNode
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode],  elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  // optional
  def receive = normal

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = {
    case op: Operation if op.elem < elem && subtrees.keySet.contains(Left) => subtrees(Left) ! op
    case op: Operation if op.elem > elem && subtrees.keySet.contains(Right) => subtrees(Right) ! op
    case op: Insert if op.elem == elem =>
      removed = false
      op.requester ! OperationFinished(op.id)
    case op: Insert =>
      subtrees = subtrees.updated(side(op.elem), newTree(op.elem))
      op.requester ! OperationFinished(op.id)
    case op: Contains => op.requester ! ContainsResult(op.id, op.elem == elem && !removed)
    case op: Remove if op.elem == elem =>
      removed = true
      op.requester ! OperationFinished(op.id)
    case op: Remove =>
//      println(s"removing nonexisting element ${op}")
      op.requester ! OperationFinished(op.id)
    case copyTo: CopyTo =>
      if (!removed) copyTo.treeNode ! Insert(self, 1, elem) //ik wil geen id
      subtrees.values.foreach { _ ! copyTo }
      becomeCopy(subtrees.values.toSet, removed)
    case op: Operation =>
      println(s"impossible operation $op ignoring")
    case a =>
      println(s"unknown case $a")
      ???
  }

  def side(opElem: Int): Position = {
    if (opElem < elem) Left
    else Right
  }

  def newTree(e: Int) = context.actorOf(props(e, initiallyRemoved = false))

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case CopyFinished => becomeCopy(expected - sender(), insertConfirmed)
    case OperationFinished(_) => becomeCopy(expected, true) //van de insert
    case a =>
      println(s"unknown case $a")
      ???
  }

  //hulpfunctie om te kijken of we klaar zijn
  def becomeCopy(expected: Set[ActorRef], insertConfirmed: Boolean): Unit = {
    println(s"becomeCopy ${self.path} $expected $insertConfirmed")
    if (expected.isEmpty && insertConfirmed) {
      context.parent ! CopyFinished
      self ! PoisonPill
    } else {
      context.become(copying(expected, insertConfirmed))
    }
  }


}
