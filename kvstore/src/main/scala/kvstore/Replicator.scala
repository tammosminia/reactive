package kvstore

import akka.actor.{ActorLogging, Props, Actor, ActorRef}
import akka.event.LoggingReceive
import akka.persistence.AtLeastOnceDelivery
import scala.concurrent.duration._

object Replicator {
  case class Replicate(key: String, valueOption: Option[String], id: Long)
  case class Replicated(key: String, id: Long)
  
  case class Snapshot(key: String, valueOption: Option[String], seq: Long)
  case class SnapshotAck(key: String, seq: Long)

  case object Tick

  def props(replica: ActorRef): Props = Props(new Replicator(replica))
}

class Replicator(val replica: ActorRef) extends Actor with ActorLogging {
  import Replicator._
  import Replica._
  import context.dispatcher
  
  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */

  // map from sequence number to pair of sender and request
  var acks = Map.empty[Long, (ActorRef, Replicate)]
  // a sequence of not-yet-sent snapshots (you can disregard this if not implementing batching)
  var pending = Vector.empty[Snapshot]
  
  var _seqCounter = 0L
  def nextSeq = {
    val ret = _seqCounter
    _seqCounter += 1
    ret
  }

  context.system.scheduler.schedule(0.seconds, 100.millisecond, self, Tick)

  /* TODO Behavior for the Replicator. */
  def receive: Receive = LoggingReceive {
    case r@Replicate(key, value, id) =>
      val seq = nextSeq
      acks = acks.updated(seq, (sender(), r))
      replica ! Snapshot(key, value, seq)
    case SnapshotAck(key, seq) =>
      val (s, r) = acks(seq)
      acks = acks - seq
      s ! Replicated(key, r.id)
    case Tick =>
      acks.foreach { case (seq, (ref, Replicate(key, value, id))) =>
        println(s"resending $seq")
        replica ! Snapshot(key, value, seq)
      }
  }


}
