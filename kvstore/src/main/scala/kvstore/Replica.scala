package kvstore

import akka.actor.{ OneForOneStrategy, Props, ActorRef, Actor }
import kvstore.Arbiter._
import scala.collection.immutable.Queue
import akka.actor.SupervisorStrategy.Restart
import scala.annotation.tailrec
import akka.pattern.{ ask, pipe }
import akka.actor.Terminated
import scala.concurrent.duration._
import akka.actor.PoisonPill
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  case class TickTimeout(id: Long)

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {
  import Replica._
  import Replicator._
  import Persistence._
  import context.dispatcher

  /*
   * The contents of this actor is just a suggestion, you can implement it in any way you like.
   */
  
  var kv = Map.empty[String, String]
  // a map from secondary replicas to replicators
  var secondaries = Map.empty[ActorRef, ActorRef]
  // the current set of replicators
  var replicators = Set.empty[ActorRef]
  val persistanceActor = context.actorOf(persistenceProps)
  var persistings = Map.empty[Long, (ActorRef, Persist)] // Id, (sender, message)
  var replicatings = Map.empty[Long, (ActorRef, Replicator.Replicate, Set[ActorRef])] // id, (sender, message, remainingRecipients)

  arbiter ! Join

  context.system.scheduler.schedule(0.seconds, 100.millisecond, self, Tick)

  def receive = {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica(0))
  }

  /* TODO Behavior for  the leader role. */
  val leader: Receive = {
    case Insert(key, value, id) =>
      updateAndPersist(key, Some(value), id)
      context.system.scheduler.scheduleOnce(1.second, self, TickTimeout(id))
      replicate(key, Some(value), id)
    case Remove(key, id) =>
      updateAndPersist(key, None, id)
      context.system.scheduler.scheduleOnce(1.second, self, TickTimeout(id))
      replicate(key, None, id)
    case Get(key, id) =>
      sender() ! GetResult(key, kv.get(key), id)
    case Persisted(key, id) =>   //TODO: als de timeout al voorbij is
//      println(s"Persisted $key, $id")
      val (s, persist) = persistings(id)
      persistings -= id
      checkDone(id, s)
    case Replicated(key, id) =>
//      println(s"Replicated $key, $id")
      val (s, snapshot, reps) = replicatings(id)
      val newReps = reps - sender()
      if (newReps.isEmpty) {
        replicatings -= id
        checkDone(id, s)
      } else {
        replicatings = replicatings.updated(id, (s, snapshot, newReps))
      }
    case Tick =>
      println(s"Tick - $persistings $replicatings")
      persistings.foreach { case (seq, (ref, persist)) =>
        persistanceActor ! persist
      }
      replicatings.foreach { case (seq, (ref, snapshot, reps)) =>
        reps.foreach { rep =>
          rep ! snapshot
        }
      }
    case TickTimeout(id) =>
//      println(s"TickTimeout $id")
      val s = senderForId(id)
      persistings -= id
      replicatings -= id
      s ! OperationFailed(id)
    case Replicas(replicas) =>
      (replicas - self).foreach { replica =>
//        println(s"Replicas $replica")
        val replicator = context.actorOf(Replicator.props(replica))
        secondaries = secondaries.updated(replica, replicator)
        replicators = replicators + replicator
      }
  }


  /* TODO Behavior for the replica role. */
  def replica(expectedSeq: Int): Receive = {
    case Get(key, id) =>
      sender() ! GetResult(key, kv.get(key), id)
    case Snapshot(key, value, seq) if seq == expectedSeq =>
      updateAndPersist(key, value, seq)
      context.become(replica(expectedSeq + 1))
    case Persisted(key, id) =>
      val (s, persist) = persistings(id)
      s ! SnapshotAck(key, id)
    case Snapshot(key, _, seq) if seq < expectedSeq =>
      sender() ! SnapshotAck(key, seq)
    case Snapshot(_, _, seq) if seq > expectedSeq =>
      //We doen niks met toekomstige dingen. Die worden geretryed
    case Tick =>
      persistings.foreach { case (seq, (ref, persist)) =>
          persistanceActor ! persist
      }
  }

  def updateKV(key: String, value: Option[String]) = value match {
    case Some(v) => kv = kv.updated(key, v)
    case None => kv = kv - key
  }

  def updateAndPersist(key: String, value: Option[String], seq: Long) = {
    updateKV(key, value)
    persistings = persistings.updated(seq, (sender(), Persist(key, value, seq)))
    persistanceActor ! Persist(key, value, seq)
  }

  def replicate(key: String, value: Option[String], seq: Long) = if (!replicators.isEmpty) {
    val r = Replicate(key, value, seq)
    replicatings = replicatings.updated(seq, (sender(), r, replicators))
    replicators.foreach { replicator =>
      replicator ! r
    }
  }

  def checkDone(id: Long, client: ActorRef) = {
    if (!persistings.contains(id) && !replicatings.contains(id)) {
      client ! OperationAck(id)
    }
  }

  def senderForId(id: Long): ActorRef = persistings.get(id) match {
    case Some((s, _)) => s
    case None =>
      val (s, _, _) = replicatings(id)
      s
  }
}

